package serializer

import (
	"testing"
	"fmt"
)

type settings struct {
	Id int
	FirstName string
	MiddleNames []string
	LastName string
}

func TestLoadSave(t *testing.T) {
	var filename = "settings.json"

	// save
	var s = &settings{}
	s.Id = 5
	s.FirstName = "John"
	s.MiddleNames = []string{"James", "Percival", "Marlboro"}
	s.LastName = "Doe"
	err := Save(s, filename);
	if err != nil {
		t.Error("Error encountered: %s", err)
	}

	// load
	var s2 *settings
	err = Load(&s2, filename)
	if err != nil {
		t.Error("Error encountered: %s", err)
	}
	var expected = "&{Id:5 FirstName:John MiddleNames:[James Percival Marlboro] LastName:Doe}"
	var actual = fmt.Sprintf("%+v", s)
	if expected != actual {
		t.Error("Error: '" + actual + "' does not have the expected value '" + expected +"'" )
	}
}

