package serializer

import (
	"encoding/json"
	"io/ioutil"
)
var filename_default = "settings.json"
type Settings interface {

}

func Save(s Settings, filename string) error {
	if filename == "" {
		filename = filename_default
	}
	json, err := json.Marshal(s)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(filename, json, 0600)
}

func Load(s Settings, filename string) error {
	if filename == "" {
		filename = filename_default
	}
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	json.Unmarshal(data, s)
	return nil
}
