# go-serializer #

## Install

`go get bitbucket.org/dasvdk/go-serializer`

## Usage

### main.go

```go
package main

import (
	"fmt"
	"bitbucket.org/dasvdk/go-serializer"
)

type settings struct {
	Id int
	FirstName string
	MiddleNames []string
	LastName string
}

func main() {
	var filename = "settings.json"

	// save
	var s = &settings{}
	s.Id = 5
	s.FirstName = "John"
	s.MiddleNames = []string{"James", "Percival", "Marlboro"}
	s.LastName = "Doe"
	serializer.Save(s, filename);

	// load
	var s2 = &settings{}
	serializer.Load(&s2, filename)
	fmt.Println(s2)
}
```

Output
`&{5 John [James Percival Marlboro] Doe}`